package Controller;

import Model.CinemaComplex;
import Model.Zaal;
import Persistence.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 30/10/12
 * Time: 10:31
 * To change this template use File | Settings | File Templates.
 */
public class CinemaComplexController {
    private static CinemaComplexController instance = null;


    protected CinemaComplexController() {
    }

    public static CinemaComplexController getInstance() {
        if (instance == null) {
            instance = new CinemaComplexController();
        }
        return instance;
    }
    public void voegToe(String naam,String adres){
        CinemaComplex cinemaComplex = new CinemaComplex(naam,adres);
        save(cinemaComplex);
    }
    public void voegToe(CinemaComplex cinemaComplex){
        save(cinemaComplex);
    }
    public Vector<CinemaComplex> getCinemaComplexes(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from CinemaComplex c");
        Vector<CinemaComplex> complexes = new Vector<CinemaComplex>(query.list());

        session.close();
        return complexes;
    }
    public CinemaComplex getCinemaComplexByName(String naam){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from CinemaComplex c join fetch c.zalen z where c.naam = :naam");
        query.setString("naam", naam);
        List<CinemaComplex> complexes= new ArrayList<CinemaComplex>(query.list());

        session.close();
        return complexes.get(0);
    }
    public void addZaal(Zaal zaal,String naam){
        CinemaComplex cinemaComplex = getCinemaComplexByName(naam);
        cinemaComplex.addZaal(zaal);
        save(cinemaComplex);
    }
    public void save(Object o) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(o);
        tx.commit();
        session.close();
    }
}
