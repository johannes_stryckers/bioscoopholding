package Controller;


import Model.*;
import Persistence.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.*;


/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:51
 * To change this template use File | Settings | File Templates.
 */
public class Systeem {
    private static Systeem instance = null;


    protected Systeem() {
    }

    public static Systeem getInstance() {
        if (instance == null) {
            instance = new Systeem();
        }
        return instance;
    }

    public void voegUitgeverToe(String adres, String naam, Date oprichtingsJaar){
        Uitgever uitgever = new Uitgever( adres,  naam,  oprichtingsJaar);
        save(uitgever);
    }

    public Vector<Zetel> getBeschikbarePlaatsenPerVertoning(Vertoning vertoning){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Zetel z where z.zaal = :zaal and z.id not in (Select t.zetel.id from Ticket t where t.vertoning = :vertoning)");
        query.setEntity("zaal", vertoning.getZaal());
        query.setEntity("vertoning", vertoning);

        Vector<Zetel> zetels = new Vector<Zetel>(query.list());

        session.close();
        return zetels;
    }

    public Vector<Vertoning> getKomendeVertoningen(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Vertoning as v where v.zetelReservatie = true and v.date > :date");
        query.setTimestamp("date", new Date());

        Vector<Vertoning> vertoningen = new Vector<Vertoning>(query.list());

        session.close();
        return vertoningen;
    }

    public Vector<Medewerker> getActeurs(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Medewerker as m where m.id in (select f.medewerker.id from Functie f where f.type = :functie)");
        query.setString("functie", "Acteur");

        Vector<Medewerker> medewerkers = new Vector<Medewerker>(query.list());

        session.close();
        return medewerkers;
    }

    public Vector<Vertoning> getVertoningen(Date begin,Date einde, CinemaComplex cinemaComplex){
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from Vertoning v  where v.zaal.id in (select z.id from Zaal z where z.cinemaComplex = :cinemacomplex) and v.date between :begin and :einde");
        query.setTimestamp("begin", begin);
        query.setTimestamp("einde", einde);
        query.setEntity("cinemacomplex", cinemaComplex);

        Vector<Vertoning> vertoningen= new Vector<Vertoning>(query.list());

        session.close();
        return vertoningen;
    }

    public Vector<Vertoning> getVertoningen(Date begin,Date einde, CinemaComplex cinemaComplex, Medewerker medewerker){
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from Vertoning v  where v.zaal.id in (select z.id from Zaal z where z.cinemaComplex = :cinemacomplex) and v.film.id in (select f.film.id from Functie f where f.medewerker = :medewerker and f.type = :type) and v.date between :begin and :einde");
        query.setTimestamp("begin", begin);
        query.setTimestamp("einde", einde);
        query.setEntity("medewerker", medewerker);
        query.setString("type", "Acteur");
        query.setEntity("cinemacomplex", cinemaComplex);

        Vector<Vertoning> vertoningen= new Vector<Vertoning>(query.list());

        session.close();
        return vertoningen;
    }

    public Zetel getZetelByNr(Vertoning vertoning, int nr){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Zetel z where z.zetelNr = :nr and z.zaal.id = :v ");
        query.setParameter("nr",nr);
        query.setParameter("v",vertoning.getZaal().getId());
        ArrayList<Zetel> zetels = new ArrayList<Zetel>(query.list());

        session.close();

        return zetels.get(0);

    }
    public Bezoeker getBezoekerByName(String naam,String adres,boolean student){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Bezoeker b where b.naam = :naam and b.adres = :adres and b.student = :student");
        query.setParameter("naam",naam);
        query.setParameter("adres",adres);
        query.setParameter("student",student)   ;
        ArrayList<Bezoeker> bezoekers = new ArrayList<Bezoeker>(query.list());

        session.close();
        if(bezoekers.size()>0){
            return bezoekers.get(0);
        }
        else {
            Bezoeker bezoeker= new Bezoeker( naam,  student,  adres);
           save(bezoeker);
            return bezoeker;
        }

    }
    public Vector<Uitgever> getUitgevers() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Uitgever");
        Vector<Uitgever> uitgevers = new Vector(query.list());

        session.close();
        return uitgevers;
    }

    public void save(Object o) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(o);
        tx.commit();
        session.close();
    }
    public void merge(Object o) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.merge(o);
        tx.commit();
        session.close();
    }
    public void voegFilmToe(String naam, Integer speelDuur, Integer leeftijdsCategorie, String land, String korteInhoud, String genre, Date datum,Uitgever uitgever) {
        Film film = new Film(naam, speelDuur, leeftijdsCategorie, land, korteInhoud, genre,uitgever);
        film.setDate(datum);
        save(film);
    }
    
    public void voegZaalToe(){
        Zaal zaal = new Zaal();
        save(zaal);
    }
    
    public void voegVertoningToe(Film film, Zaal zaal,Date date){
        Vertoning vertoning=new Vertoning(film,zaal,date);
        save(vertoning);
    }
    
    public List<Film> getFilmsByName(String naam){
        Session session = HibernateUtil.getSessionFactory().openSession();

        Query query = session.createQuery("from Film f left join fetch f.mediaBerichten where f.naam = :naam");
        query.setString("naam", naam);
        ArrayList<Film> films = new ArrayList<Film>(query.list());
        session.close();
        return films;
    }

}
