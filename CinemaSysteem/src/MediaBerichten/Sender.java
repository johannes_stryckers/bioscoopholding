package MediaBerichten;

import Model.MediaBericht;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.exolab.castor.xml.Marshaller;
import javax.jms.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: Bart
 * Date: 30/10/12
 * Time: 20:58
 * To change this template use File | Settings | File Templates.
 */

public class Sender {
    public static void main(String[] args) {

        try{

            String[] socialSites = {"Facebook", "Twitter", "Netlog", "Wordpress", "LinkedIn", "Hyves"};
            String inhoud = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.";
            String separator =System.getProperty("file.separator");
            Random rnd = new Random();
            File file = new File("CinemaSysteem"+separator+"src"+separator+"Data"+separator+"Files"+separator+"FilmTitels.csv");
            ArrayList<String> titels = new ArrayList<String>();

            BufferedReader bufRdr  = new BufferedReader(new FileReader(file));
            String line = null;
            while((line = bufRdr.readLine()) != null)
            {
                titels.add(line);
            }
            
            // Create a ConnectionFactory
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("failover:(tcp://localhost:61616,tcp://localhost:61617)?randomize=false");
            // Create a Connection to ActiceMQ
            Connection connection = connectionFactory.createConnection();
            connection.start();
            // Create a Session that allows you to work with activeMQ
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            // Create the destination queue (or retrieve it, if it already exists)
            Destination destination = session.createQueue("TEST.SENDRECEIVE");
            // Create a MessageProducer for the Destination
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            StringWriter stringWriter = new StringWriter();
            Marshaller marshaller = new Marshaller(stringWriter);

            int i=0;

            do{
                stringWriter.getBuffer().setLength(0);
                MediaBericht bericht = new MediaBericht();
                bericht.setDatum(new Date());
                bericht.setSociaalNetwerk(socialSites[i%socialSites.length]);
                bericht.setInhoud(inhoud);
                int titel = rnd.nextInt(titels.size());
                System.out.println("Mediabericht verzonden: " + titels.get(titel));
                bericht.setFilmNaam(titels.get(titel));
                titels.remove(titel);
                
                marshaller.marshal(bericht);

                TextMessage message = session.createTextMessage(stringWriter.toString());

                producer.send(message);

                Thread.sleep(10000);
                i++;
            }while (titels.size() != 0);

            producer.close();
            session.close();
            connection.close();

        }catch (Exception e){
            System.err.println(e.getCause() + "\n" + e.getMessage());
        }
    }
}
