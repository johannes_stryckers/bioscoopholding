import Controller.Systeem;
import Data.DummyData;
import Model.Film;
import Model.MediaBericht;
import View.BioscoopHolding;
import XML.CreateXml;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.exolab.castor.xml.Unmarshaller;
import org.xml.sax.InputSource;

import javax.jms.*;
import java.io.StringReader;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:52
 * To change this template use File | Settings | File Templates.
 */

public class Main {
    public static void main(String[] args) {

        Systeem controller = Systeem.getInstance();
        //DummyData dummyData = new DummyData();
        BioscoopHolding view = new BioscoopHolding(controller);
        //CreateXml xml = new CreateXml();
        //xml.CreateFile();

        try{
            // Create a ConnectionFactory
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("failover:(tcp://localhost:61616,tcp://localhost:61617)?randomize=false");
            // Create a Connection to ActiceMQ
            Connection connection = connectionFactory.createConnection();
            connection.start();
            // Create a Session that allows you to work with activeMQ
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            // Create the destination queue (or retrieve it, if it already exists)
            Destination destination = session.createQueue("TEST.SENDRECEIVE");

            MessageConsumer consumer = session.createConsumer(destination);

            TextMessage textMessage;

            do{
                Message message = consumer.receive();
                textMessage = (TextMessage) message;
                StringReader sr = new StringReader(textMessage.getText());
                InputSource is = new InputSource(sr);
                Unmarshaller um = new Unmarshaller();
                MediaBericht bericht = (MediaBericht) um.unmarshal(MediaBericht.class, is);
                System.out.println("Bericht aangekomen: " + bericht.getFilmNaam());

                for(Film film: controller.getFilmsByName(bericht.getFilmNaam())){
                    film.addMediaBericht(bericht);
                    controller.save(film);
                    System.out.println("Film was gevonden en bericht is opgeslagen");
                }

            }while(textMessage.getText() != null);

            consumer.close();
            session.close();
            connection.close();

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
