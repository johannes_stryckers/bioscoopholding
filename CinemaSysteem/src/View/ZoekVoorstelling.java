package View;

import Controller.CinemaComplexController;
import Controller.Systeem;
import Model.CinemaComplex;
import Model.Medewerker;
import Model.Uitgever;
import Model.Vertoning;
import View.CustomSwingComponents.RegexFormatter;
import com.sun.deploy.config.WebStartConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 25/10/12
 * Time: 12:24
 * To change this template use File | Settings | File Templates.
 */
public class ZoekVoorstelling extends JFrame{
    private CinemaComplexController complexController= CinemaComplexController.getInstance();
    Systeem systeem= Systeem.getInstance();
    private JLabel van, tot;

    private JSpinner beginTijd, eindTijd;
    private JComboBox<Medewerker> acteurs;
    private JList<Vertoning> voorstellingen;
    private JButton zoek;
    private JComboBox<CinemaComplex> CmbComplex;
    
    public ZoekVoorstelling(){
        super("Zoek Voorstelling");
        initialiseerComponenten();
        layoutComponenten();
        maakEventHandlers();
        toonFrame();
    }

    private void initialiseerComponenten() {
        van = new JLabel("Van: (dd/mm/yyyy)");
        tot = new JLabel("Tot: (dd/mm/yyyy)");
        CmbComplex = new JComboBox<CinemaComplex>(complexController.getCinemaComplexes());
        beginTijd = new JSpinner(new SpinnerDateModel());
        eindTijd = new JSpinner(new SpinnerDateModel());
        acteurs = new JComboBox<Medewerker>();
        voorstellingen = new JList<Vertoning>();
        zoek = new JButton("Zoek Voorstellingen");
        acteurs = new JComboBox<Medewerker>(systeem.getActeurs());
    }

    private void layoutComponenten() {

        Date date = new Date();
        
        JSpinner.DateEditor beginEditor = new JSpinner.DateEditor(beginTijd, "dd/MM/yyyy HH:mm");
        JSpinner.DateEditor eindEditor = new JSpinner.DateEditor(eindTijd, "dd/MM/yyyy HH:mm");
        beginTijd.setEditor(beginEditor);
        beginTijd.setValue(date);
        eindTijd.setEditor(eindEditor);

        JPanel center = new JPanel(new GridLayout(1,2));

        JPanel west = new JPanel(new BorderLayout());
        west.add(van, BorderLayout.NORTH);
        west.add(beginTijd, BorderLayout.SOUTH);

        JPanel east = new JPanel(new BorderLayout());
        east.add(tot, BorderLayout.NORTH);
        east.add(eindTijd, BorderLayout.SOUTH);

        center.add(west);
        center.add(east);

        JPanel south = new JPanel(new BorderLayout());
        south.add(zoek, BorderLayout.NORTH);
        JScrollPane pane = new JScrollPane(voorstellingen);
        south.add(pane);

        JPanel p = new JPanel(new BorderLayout());
        p.add(CmbComplex, BorderLayout.NORTH);
        p.add(acteurs, BorderLayout.CENTER);

        this.add(center, BorderLayout.NORTH);
        this.add(p, BorderLayout.CENTER);
        this.add(south, BorderLayout.SOUTH);
    }

    private void maakEventHandlers() {
        zoek.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date begin = (Date)beginTijd.getValue();
                Date einde =(Date) eindTijd.getValue();
                voorstellingen.clearSelection();
                voorstellingen.setListData(systeem.getVertoningen(begin,einde,(CinemaComplex) CmbComplex.getSelectedItem(), (Medewerker) acteurs.getSelectedItem()));
            }
        });
    }
    public void updateList(JList list, ArrayList objs)//your standard list update method
    {
        DefaultListModel listModel = (DefaultListModel)list.getModel();
        listModel.clear();
        for(int i=0; i<objs.size(); i++)
        {
            listModel.addElement((String)objs.get(i));
        }
    }
    private void toonFrame() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenSize.width/2 - 200, screenSize.height/2 - 200);
        this.setSize(500,270);
        this.setResizable(false);
        this.setVisible(true);
    }
}
