package View;

import javax.swing.*;

import Controller.CinemaComplexController;
import Controller.Systeem;
import Model.Uitgever;
import View.CustomSwingComponents.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 25/10/12
 * Time: 12:23
 * To change this template use File | Settings | File Templates.
 */
public class VoegFilmToe extends JFrame {

    private JButton voegToe, cancel;
    private JLabel naam, leeftijdsCategorie, speelduur, land, genre, uitgever;
    private JTextField txtNaam, txtLand, txtGenre;
    private JFormattedTextField txtLeeftijdsCategorie, txtSpeelDuur;
    private JTextArea txtKorteInhoud;
    private JCheckBox  chkOndertiteld;
    private JComboBox<Uitgever> uitgevers;
    private Systeem controller = Systeem.getInstance();

    public VoegFilmToe() {
        super("Voeg Film Toe");
        initialiseerComponenten();
        layoutComponenten();
        maakEventHandlers();
        toonFrame();
    }

    private void initialiseerComponenten() {
        voegToe = new JButton("Voeg Toe");
        cancel = new JButton("Cancel");

        naam = new JLabel("Naam:");
        leeftijdsCategorie = new JLabel("Leeftijds Categorie: (cijfer>0)");
        speelduur = new JLabel("Speelduur: (cijfer>0)");
        land = new JLabel("Land:");
        genre = new JLabel("Genre:");
        uitgever = new JLabel("Uitgever:");
        uitgevers = new JComboBox<Uitgever>(controller.getUitgevers());

        txtNaam = new JTextField();
        txtGenre = new JTextField();
        txtKorteInhoud = new JTextArea();
        txtLand = new JTextField();
        txtLeeftijdsCategorie = new JFormattedTextField(new RegexFormatter("[1-9]\\d*"));
        txtSpeelDuur = new JFormattedTextField(new RegexFormatter("[1-9]\\d*"));
        txtKorteInhoud = new JTextArea();

        chkOndertiteld = new JCheckBox("Ondertiteld");
    }

    private void layoutComponenten() {

        txtNaam.setToolTipText("Naam van de film");

        JScrollPane scrollPane = new JScrollPane(txtKorteInhoud);
        txtKorteInhoud.setLineWrap(true);
        txtKorteInhoud.setWrapStyleWord(true);
        txtKorteInhoud.setSize(40, 200);
        txtKorteInhoud.setText("Korte inhoud:");

        GridLayout layout = new GridLayout(7,2);
        JPanel north = new JPanel(layout);
        JPanel south = new JPanel();

        north.add(naam);
        north.add(txtNaam);
        north.add(leeftijdsCategorie);
        north.add(txtLeeftijdsCategorie);
        north.add(speelduur);
        north.add(txtSpeelDuur);
        north.add(land);
        north.add(txtLand);
        north.add(genre);
        north.add(txtGenre);
        north.add(uitgever);
        north.add(uitgevers);

        north.add(chkOndertiteld);
        this.add(north, BorderLayout.NORTH);
        this.add(scrollPane, BorderLayout.CENTER);
        south.add(voegToe, BorderLayout.EAST);
        south.add(cancel, BorderLayout.WEST);
        this.add(south, BorderLayout.SOUTH);
    }

    private void maakEventHandlers() {
        this.voegToe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validate();

                controller.voegFilmToe(txtNaam.getText(),Integer.parseInt(txtSpeelDuur.getText()),Integer.parseInt(txtLeeftijdsCategorie.getText()),txtLand.getText(),txtKorteInhoud.getText(),txtGenre.getText(), new Date(),(Uitgever)uitgevers.getSelectedItem());
                JOptionPane.showMessageDialog(null,"film toegevoegd");
                VoegFilmToe.this.dispose();
            }
        });
        this.cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VoegFilmToe.this.dispose();
            }
        });
    }

    private void toonFrame() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenSize.width/2 - 200, screenSize.height/2 - 200);
        this.setSize(400, 400);
        this.setResizable(false);
        this.setVisible(true);
    }
}
