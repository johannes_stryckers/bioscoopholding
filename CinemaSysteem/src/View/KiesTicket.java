package View;

import Controller.Systeem;
import Model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Bart
 * Date: 30/10/12
 * Time: 23:01
 * To change this template use File | Settings | File Templates.
 */
public class KiesTicket extends JFrame{

    private JComboBox<Vertoning> vertoningen;
    private JComboBox<Zetel> zetelNr;
    private JLabel vertoning, zetel;
    private JButton ok;
    private JCheckBox student;
    private Vector<Vertoning> vertoningVector;
    private int zaalNr;
    private KoopTickets koopTickets;
    Systeem systeem = Systeem.getInstance();

    public KiesTicket(Vector<Vertoning> vertoningVector, int zaalNr,KoopTickets koopTickets){
        super("Kies Ticket");
        this.vertoningVector = vertoningVector;
        this.zaalNr = zaalNr;
        initialiseerComponenten();
        layoutComponenten();
        maakEventHandlers();
        toonFrame();
        this.koopTickets=koopTickets;
    }

    private void initialiseerComponenten() {
        vertoningen = new JComboBox<Vertoning>(vertoningVector);
        zetelNr = new JComboBox<Zetel>();
        vertoning = new JLabel("Kies een vertoning: ");
        zetel = new JLabel("Kies een zetelNr: ");
        zetel.setToolTipText("(1-20=Handicap, 21-80=Regular, 81-100=LoveSeat)");
        zetelNr.setToolTipText("(1-20=Handicap, 21-80=Regular, 81-100=LoveSeat)");
        ok = new JButton("Ok");
        student = new JCheckBox("Studenten Korting");
    }

    private void layoutComponenten() {
        JPanel north = new JPanel(new BorderLayout());
        JPanel east = new JPanel(new BorderLayout());
        JPanel west = new JPanel(new BorderLayout());
        east.add(vertoningen, BorderLayout.NORTH);
        east.add(zetelNr, BorderLayout.CENTER);
        west.add(vertoning, BorderLayout.NORTH);
        west.add(zetel, BorderLayout.CENTER);
        north.add(east, BorderLayout.CENTER);
        north.add(west, BorderLayout.WEST);
        this.add(north, BorderLayout.NORTH);
        this.add(student, BorderLayout.CENTER);
        this.add(ok, BorderLayout.SOUTH);
    }

    private void maakEventHandlers() {
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               okClick();
            }
        });
        vertoningen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Vertoning vertoning = (Vertoning)vertoningen.getSelectedItem();
                zetelNr.setModel(new DefaultComboBoxModel<Zetel>(systeem.getBeschikbarePlaatsenPerVertoning(vertoning)));
            }
        });
    }
    private void okClick(){
        Vertoning vertoning = (Vertoning) vertoningen.getSelectedItem();
        Zetel z = (Zetel)zetelNr.getSelectedItem();
        boolean isStudent= student.isSelected();
        vertoning.setZaal(z.getZaal());
        koopTickets.afrekening.addTicket(new Ticket((new Date().getTime())+ "" + z.getId(), z.getZone().getPrijs(),  isStudent, z,vertoning) );
        koopTickets.update();
        this.dispose();
    }
    private void toonFrame() {
        //vertoningen.setSelectedIndex(0);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenSize.width/2 - 200, screenSize.height/2 - 200);
        this.setSize(700,150);
        this.setResizable(true);
        this.setVisible(true);
    }

}
