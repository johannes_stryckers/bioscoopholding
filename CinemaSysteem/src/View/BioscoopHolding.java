package View;

import Controller.Systeem;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 23/10/12
 * Time: 13:22
 * To change this template use File | Settings | File Templates.
 */
public class BioscoopHolding extends JFrame {

    private JButton voegFilmToe, koopTicket, zoekVoortselling, zoekBeschikbarePlaatsen;

    private Systeem controller;

    public BioscoopHolding(Systeem controller) throws HeadlessException {
        super("BioscoopHolding");
        this.controller = controller;
        initialiseerComponenten();
        layoutComponenten();
        maakEventHandlers();
        toonFrame();
    }

    private void initialiseerComponenten() {
        this.voegFilmToe = new JButton("Voeg film toe");
        this.koopTicket = new JButton("Koop tickets");
        this.zoekVoortselling = new JButton("Zoek voorstelling");
        this.zoekBeschikbarePlaatsen = new JButton("Zoek beschikbare plaatsen");
    }

    private void layoutComponenten() {

        this.setLayout(new GridLayout(2,2));

        this.add(voegFilmToe);
        this.add(koopTicket);
        this.add(zoekVoortselling);
        this.add(zoekBeschikbarePlaatsen);
    }

    private void maakEventHandlers() {
        this.voegFilmToe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new VoegFilmToe();
            }
        });
        this.zoekVoortselling.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ZoekVoorstelling();
            }
        });
        this.koopTicket.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new KoopTickets();
            }
        });
        this.zoekBeschikbarePlaatsen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ZoekBeschikbarePlaatsen();
            }
        });
    }

    private void toonFrame() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenSize.width/2 - 200, screenSize.height/2 - 100);
        this.setSize(400, 200);
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
