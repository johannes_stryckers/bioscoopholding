package View;

import Controller.Systeem;
import Model.Vertoning;
import Model.Zetel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 25/10/12
 * Time: 12:24
 * To change this template use File | Settings | File Templates.
 */
public class ZoekBeschikbarePlaatsen extends JFrame{

    private JComboBox<Vertoning> vertoningen;
    private JLabel vertoning, plaatsen;
    private JButton zoek;
    private JList<Zetel> zetels;
    private Systeem controller;

    public ZoekBeschikbarePlaatsen(){
        super("Zoek Beschibare Plaatsen");
        initialiseerComponenten();
        layoutComponenten();
        maakEventHandlers();
        toonFrame();
    }

    private void initialiseerComponenten() {
        controller = Systeem.getInstance();
        vertoningen = new JComboBox<Vertoning>(controller.getKomendeVertoningen());
        vertoning = new JLabel("Vertoningen met reservatie: ");
        plaatsen = new JLabel("Beschikbare Plaatsen: ");
        zoek = new JButton("Zoek Plaatsen");
        zetels = new JList<Zetel>();
    }

    private void layoutComponenten() {
        zetels.setSize(200, 200);
        JPanel west = new JPanel(new BorderLayout());
        west.add(vertoning, BorderLayout.NORTH);
        west.add(plaatsen, BorderLayout.SOUTH);
        JPanel east = new JPanel(new BorderLayout());
        east.add(vertoningen, BorderLayout.NORTH);
        JScrollPane scrollPane = new JScrollPane(zetels);
        east.add(scrollPane, BorderLayout.SOUTH);
        this.add(east, BorderLayout.CENTER);
        this.add(west, BorderLayout.WEST);
        this.add(zoek, BorderLayout.SOUTH);
    }

    private void maakEventHandlers() {
        zoek.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                zetels.clearSelection();
                zetels.setListData(controller.getBeschikbarePlaatsenPerVertoning((Vertoning)vertoningen.getSelectedItem()));
            }
        });
    }


    private void toonFrame() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenSize.width/2 - 200, screenSize.height/2 - 200);
        this.setSize(800,220);
        this.setResizable(false);
        this.setVisible(true);
    }


}
