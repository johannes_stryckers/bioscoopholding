package View;

import Controller.CinemaComplexController;
import Controller.Systeem;
import Model.Afrekening;
import Model.CinemaComplex;
import Model.Ticket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 25/10/12
 * Time: 12:23
 * To change this template use File | Settings | File Templates.
 */
public class KoopTickets extends JFrame {

    private JButton voegTicketToe, klaar, cancel;
    private JSpinner txtZaalNr;
    private JLabel aantalTickets, totaalPrijs, txtTotaalTickets, txtTotaalPrijs, zaalNr, cinemacomplex, naam, adres;
    private JTextField txtNaam, txtAdres;
    private JCheckBox student;
    private JComboBox<CinemaComplex> CmbComplex;
    private CinemaComplexController cinemaComplexController;
    protected Afrekening afrekening = new Afrekening(new Date());
    Systeem systeem= Systeem.getInstance();

    private JLabel info, van, tot, acteur, lVoorstellingen;
    private JSpinner beginTijd, eindTijd;
    private Date today = new Date();
    public KoopTickets() {
        super("Koop Tickets");
        initialiseerComponenten();
        layoutComponenten();
        maakEventHandlers();
        toonFrame();

    }

    private void initialiseerComponenten() {
        voegTicketToe = new JButton("Voeg Ticket Toe");
        klaar = new JButton("Klaar");
        cancel = new JButton("Cancel");
        aantalTickets = new JLabel("Aantal Tickets: ");
        totaalPrijs = new JLabel("Totaal Prijs: €");
        zaalNr = new JLabel("Zaal Nr: ");
        cinemacomplex = new JLabel("CinemaComplex: ");
        txtZaalNr = new JSpinner(new SpinnerNumberModel(1,1,20,1));
        txtTotaalPrijs = new JLabel("0");
        txtTotaalTickets = new JLabel("0");
        naam = new JLabel("Naam: ");
        adres = new JLabel("Adres: ");
        txtNaam = new JTextField();
        txtAdres = new JTextField();
        student = new JCheckBox("Student");
        cinemaComplexController = CinemaComplexController.getInstance();
        CmbComplex = new JComboBox<CinemaComplex>(cinemaComplexController.getCinemaComplexes());

        van = new JLabel("Van: (dd/mm/yyyy)");
        tot = new JLabel("Tot: (dd/mm/yyyy)");
        beginTijd = new JSpinner(new SpinnerDateModel());
        eindTijd = new JSpinner(new SpinnerDateModel());
    }

    private void layoutComponenten() {
        Date date = new Date();

        JSpinner.DateEditor beginEditor = new JSpinner.DateEditor(beginTijd, "dd/MM/yyyy HH:mm");
        JSpinner.DateEditor eindEditor = new JSpinner.DateEditor(eindTijd, "dd/MM/yyyy HH:mm");
        beginTijd.setEditor(beginEditor);
        beginTijd.setValue(date);
        eindTijd.setEditor(eindEditor);

        JPanel dat = new JPanel(new GridLayout(1,2));

        JPanel west = new JPanel(new BorderLayout());
        west.add(van, BorderLayout.NORTH);
        //west.add(beginDatum, BorderLayout.CENTER);
        west.add(beginTijd, BorderLayout.SOUTH);

        JPanel east = new JPanel(new BorderLayout());
        east.add(tot, BorderLayout.NORTH);
        //east.add(eindDatum, BorderLayout.CENTER);
        east.add(eindTijd, BorderLayout.SOUTH);

        dat.add(west);
        dat.add(east);

        JPanel south = new JPanel(new BorderLayout());
        south.add(voegTicketToe, BorderLayout.WEST);
        south.add(klaar, BorderLayout.CENTER);
        south.add(cancel, BorderLayout.EAST);
        JPanel temp = new JPanel(new BorderLayout());
        this.add(south, BorderLayout.SOUTH);
        JPanel center = new JPanel(new BorderLayout());
        JPanel x = new JPanel(new BorderLayout());
        x.add(cinemacomplex, BorderLayout.WEST);
        x.add(CmbComplex, BorderLayout.CENTER);
        center.add(x, BorderLayout.NORTH);
        //center.add(zaalNr);
        //center.add(txtZaalNr);
        JPanel y = new JPanel(new GridLayout(1,4));
        y.add(aantalTickets);
        y.add(txtTotaalTickets);
        y.add(totaalPrijs);
        y.add(txtTotaalPrijs);
        center.add(y, BorderLayout.CENTER);
        temp.add(center,BorderLayout.SOUTH);
        temp.add(dat,BorderLayout.NORTH);

        this.add(temp, BorderLayout.CENTER);
        JPanel north = new JPanel(new GridLayout(3,2));
        north.add(naam);
        north.add(txtNaam);
        north.add(adres);
        north.add(txtAdres);
        north.add(student);
        this.add(north, BorderLayout.NORTH);
    }

    private void maakEventHandlers() {
        this.cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                KoopTickets.this.dispose();
            }
        });
        this.voegTicketToe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                voegTicketToeClick();
            }
        });
        this.klaar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                klaarClick();
            }
        });
    }
    public void voegTicketToeClick(){
        Date begin = (Date)beginTijd.getValue();
        Date einde =(Date) eindTijd.getValue();

        if(begin.compareTo(today)<0|einde.compareTo(today)<0|einde.compareTo(begin)<0 ){
            JOptionPane.showMessageDialog(null,"gelieve geen datums uit het verleden te nemen alsook de einddatum niet voor de begindatum");
        }   else {
            new KiesTicket(systeem.getVertoningen(begin,einde,(CinemaComplex) CmbComplex.getSelectedItem()),(Integer) txtZaalNr.getValue(),this);
        }

    }

      public void klaarClick(){
          afrekening.setBezoeker(systeem.getBezoekerByName(txtNaam.getText(),txtAdres.getText(),student.isSelected()));
          systeem.save(afrekening);
          this.dispose();
      }

   public void update(){
       txtTotaalPrijs.setText("€"+afrekening.getPrijs());
       txtTotaalTickets.setText(""+afrekening.getTickets().size());
   }

    private void toonFrame() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenSize.width/2 - 200, screenSize.height/2 - 200);
        this.pack();
        this.setResizable(false);
        this.setVisible(true);
    }
}
