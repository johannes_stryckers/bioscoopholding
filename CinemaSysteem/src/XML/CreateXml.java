package XML;

import java.io.File;
import java.io.FileOutputStream;

import java.text.SimpleDateFormat;
import java.util.Date;

import Controller.CinemaComplexController;
import Controller.Systeem;
import Model.CinemaComplex;
import Model.Vertoning;


import org.apache.activemq.filter.XQueryExpression;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;


/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 2/11/12
 * Time: 12:56
 * To change this template use File | Settings | File Templates.
 */
public class CreateXml {
    CinemaComplexController complexController = CinemaComplexController.getInstance();
    Systeem systeem=Systeem.getInstance();
    /*
    * */
    public void CreateFile(){
        Element rootElement = new Element("CinemaHolding");
        for(CinemaComplex cinemaComplex : complexController.getCinemaComplexes() ){
            Element complex = new Element("CinemaComplex");
            complex.setAttribute("Name",cinemaComplex.getNaam());
            for(Vertoning vertoning :systeem.getVertoningen(new Date(),new Date(new Date().getTime() +1209600000),cinemaComplex) ){
                Element v = new Element("Vertoning");
                v.setAttribute("FilmNaam",vertoning.getFilm().getNaam());
                v.setAttribute("Datum", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(vertoning.getDate()));
                v.setAttribute("Zaal",vertoning.getZaal().getZaalNr().toString());
                complex.addContent(v);

            }
            rootElement.addContent(complex);
        }
        saveDocument(rootElement, "file");
    }

    public void saveDocument(Element rootElement, String fileName){

        Document document=new Document(rootElement);

        XMLOutputter outputter = new XMLOutputter();
        Format format = Format.getPrettyFormat();

        outputter.setFormat(format);
        try{
            outputter.output(document, new FileOutputStream(fileName+".xml"));
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        ;

    }

}
