package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:58
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_MEDEWERKER")
public class Medewerker {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String naam;
    private Date geboorteDatum;

    @OneToMany(mappedBy = "medewerker")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Functie> functies = new ArrayList<Functie>();

    public Medewerker() {
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setGeboorteDatum(Date geboorteDatum) {
        this.geboorteDatum = geboorteDatum;
    }

    public Integer getId() {

        return id;
    }

    public String getNaam() {
        return naam;
    }

    public Date getGeboorteDatum() {
        return geboorteDatum;
    }

    public void addFunctie(Functie functie){
        functie.setMedewerker(this);
        functies.add(functie);
    }

    @Override
    public String toString() {
        return String.format("%s", this.naam);
    }
}
