package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_ZONE")
public class Zone {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String naam;
    private Double prijs;

    @OneToMany(mappedBy = "zone")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Zetel> zetels = new ArrayList<Zetel>();

    public Zone() {
    }

    public Zone(String naam, Double prijs) {
        this.naam = naam;
        this.prijs = prijs;
    }

    public Integer getId() {
        return id;
    }

    public String getNaam() {
        return naam;
    }

    public Double getPrijs() {
        return prijs;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setPrijs(Double prijs) {
        this.prijs = prijs;
    }

    public void addZetel(Zetel zetel){
        zetel.setZone(this);
        zetels.add(zetel);
    }
}
