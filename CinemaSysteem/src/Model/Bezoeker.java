package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:53
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_BEZOEKER")
public class Bezoeker {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String naam;
    private Boolean student;
    private String adres;

    @OneToMany(mappedBy = "bezoeker")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Afrekening> afrekeningen = new ArrayList<Afrekening>();

    @OneToMany(mappedBy = "bezoeker")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Review> reviews = new ArrayList<Review>();

    public Bezoeker() {
    }

    public Bezoeker(String naam, Boolean student, String adres) {
        this.naam = naam;
        this.student = student;
        this.adres = adres;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setStudent(Boolean student) {
        this.student = student;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public Integer getId() {

        return id;
    }

    public String getNaam() {
        return naam;
    }

    public Boolean getStudent() {
        return student;
    }

    public String getAdres() {
        return adres;
    }

    public void addAfrekening(Afrekening afrekening){
        afrekening.setBezoeker(this);
        afrekeningen.add(afrekening);
    }

    public void addReview(Review review){
        review.setBezoeker(this);
        reviews.add(review);
    }
}
