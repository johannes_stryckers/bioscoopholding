package Model;

import com.sun.org.apache.bcel.internal.classfile.ConstantInterfaceMethodref;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_ZAAL")
public class Zaal {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private Integer zaalNr;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "cinemaComplexId")
    private CinemaComplex cinemaComplex;

    @OneToMany(mappedBy = "zaal")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Vertoning> vertoningen = new ArrayList<Vertoning>();

    @OneToMany(mappedBy = "zaal")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Zetel> zetels = new ArrayList<Zetel>();

    public Zaal() {
    }

    public Zaal(Integer zaalNr) {
        this.zaalNr = zaalNr;
    }

    public Zaal(Integer zaalNr, CinemaComplex cinemaComplex) {
        this.zaalNr = zaalNr;
        this.cinemaComplex = cinemaComplex;
    }

    public Integer getId() {
        return id;
    }

    public Integer getZaalNr() {
        return zaalNr;
    }

    public CinemaComplex getCinemaComplex() {
        return cinemaComplex;
    }

    public List<Vertoning> getVertoningen() {
        return vertoningen;
    }

    public List<Zetel> getZetels() {
        return zetels;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setZaalNr(Integer zaalNr) {
        this.zaalNr = zaalNr;
    }

    public void setCinemaComplex(CinemaComplex cinemaComplex) {
        this.cinemaComplex = cinemaComplex;
    }

    public void addVertoning(Vertoning vertoning){
        vertoning.setZaal(this);
        vertoningen.add(vertoning);
    }

    public void addZetel(Zetel zetel){
        zetel.setZaal(this);
        zetels.add(zetel);
    }
}
