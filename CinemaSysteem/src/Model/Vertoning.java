package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "T_VERTONING")
public class Vertoning {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private Date date;
    private boolean zetelReservatie;
    private boolean drieD;
    private boolean ondertiteld;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "zaalId")
    private Zaal zaal;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "filmId")
    private Film film;

    @OneToMany(mappedBy = "vertoning")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Ticket> tickets = new ArrayList<Ticket>();

    public Vertoning() {
    }

    public Vertoning(Film film, Zaal zaal,Date date) {
        this.film = film;
        this.zaal = zaal;
        this.date=date;
    }

    public Vertoning(Date date, boolean zetelReservatie, boolean drieD, boolean ondertiteld) {
        this.date = date;
        this.zetelReservatie = zetelReservatie;
        this.drieD = drieD;
        this.ondertiteld = ondertiteld;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setZaal(Zaal zaal) {
        this.zaal = zaal;
    }

    public Integer getId() {
        return id;
    }

    public Zaal getZaal() {
        return zaal;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public void addTicket(Ticket ticket){
        ticket.setVertoning(this);
        tickets.add(ticket);
    }

    public boolean isDrieD() {
        return drieD;
    }

    public boolean isZetelReservatie() {
        return zetelReservatie;
    }

    @Override
    public String toString() {
       return String.format("%s, wanneer: %d/%d, %d uur%s", this.film.getNaam(), this.date.getDate(), this.date.getMonth()+1, this.date.getHours(), this.zetelReservatie?", Reservatie":"");
    }
}
