package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:58
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "T_REVIEW")
public class Review {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private Date datum;
    private Integer quotering;
    @Column(columnDefinition = "TEXT")
    private String inhoud;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "filmId")
    private Film film;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "bezoekerId")
    private Bezoeker bezoeker;

    public Review() {
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public void setQuotering(Integer quotering) {
        this.quotering = quotering;
    }

    public void setInhoud(String inhoud) {
        this.inhoud = inhoud;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public void setBezoeker(Bezoeker bezoeker) {
        this.bezoeker = bezoeker;
    }

    public Integer getId() {

        return id;
    }

    public Date getDatum() {
        return datum;
    }

    public Integer getQuotering() {
        return quotering;
    }

    public String getInhoud() {
        return inhoud;
    }

    public Film getFilm() {
        return film;
    }

    public Bezoeker getBezoeker() {
        return bezoeker;
    }
}
