package Model;

import org.hibernate.annotations.Cascade;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:58
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_FILM")
public class Film {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String naam;
    private Integer speelDuur;
    private Integer leeftijdsCategorie;
    private String land;
    private Date date;

    @Column(columnDefinition="TEXT")
    private String korteInhoud;
    private String genre;


    @OneToMany(mappedBy = "film")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Vertoning> vertoningen = new ArrayList<Vertoning>();

    @OneToMany(mappedBy = "film")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<MediaBericht> mediaBerichten = new ArrayList<MediaBericht>();

    @OneToMany(mappedBy = "film")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Award> awards = new ArrayList<Award>();

    @OneToMany(mappedBy = "film")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Functie> functies = new ArrayList<Functie>();

    @OneToMany(mappedBy = "film")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Review> reviews = new ArrayList<Review>();

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "uitgeverId")
    private Uitgever uitgever;



    public Film() {
    }

    public Film(String naam, Integer speelDuur, Integer leeftijdsCategorie, String land, String korteInhoud, String genre,Uitgever uitgever) {
        this.naam = naam;
        this.speelDuur = speelDuur;
        this.leeftijdsCategorie = leeftijdsCategorie;
        this.land = land;
        this.korteInhoud = korteInhoud;
        this.genre = genre;

        this.uitgever=uitgever;
    }

    public Film(String naam, Integer speelDuur, Integer leeftijdsCategorie, String land, String korteInhoud, String genre, Boolean drieD) {
        this.naam = naam;
        this.speelDuur = speelDuur;
        this.leeftijdsCategorie = leeftijdsCategorie;
        this.land = land;
        this.korteInhoud = korteInhoud;
        this.genre = genre;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setSpeelDuur(Integer speelDuur) {
        this.speelDuur = speelDuur;
    }

    public void setLeeftijdsCategorie(Integer leeftijdsCategorie) {
        this.leeftijdsCategorie = leeftijdsCategorie;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setLand(String land) {
        this.land = land;
    }

    public void setKorteInhoud(String korteInhoud) {
        this.korteInhoud = korteInhoud;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


    public void setUitgever(Uitgever uitgever) {
        this.uitgever = uitgever;
    }

    public Integer getId() {
        return id;
    }

    public Integer getSpeelDuur() {
        return speelDuur;
    }

    public Integer getLeeftijdsCategorie() {
        return leeftijdsCategorie;
    }

    public String getLand() {
        return land;
    }

    public List<Vertoning> getVertoningen() {
        return vertoningen;
    }

    public String getKorteInhoud() {
        return korteInhoud;
    }

    public String getGenre() {
        return genre;
    }


    public Uitgever getUitgever() {
        return uitgever;
    }

    public void addVertoning(Vertoning vertoning){
        vertoning.setFilm(this);
        vertoningen.add(vertoning);
    }

    public void addMediaBericht(MediaBericht bericht){
        bericht.setFilm(this);
        mediaBerichten.add(bericht);
    }

    public void addAward(Award award){
        award.setFilm(this);
        awards.add(award);
    }

    public void addFunctie(Functie functie){
        functie.setFilm(this);
        functies.add(functie);
    }

    public void addReview(Review review){
        review.setFilm(this);
        reviews.add(review);
    }
}
