package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_TICKET")
public class Ticket {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String barcode;
    private Double prijs;
    private Boolean isStudent;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "afrekeningId")
    private Afrekening afrekening;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "zetelId")
    private Zetel zetel;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "vertoningId")
    private Vertoning vertoning;

    public Ticket() {
    }

    public Ticket(String barcode, Double prijs, Boolean student, Zetel zetel, Vertoning vertoning) {
        this.barcode = barcode;
        this.prijs = prijs;
        isStudent = student;
        if(isStudent==true){
            this.prijs -=2;
        }

        this.zetel = zetel;
        this.vertoning = vertoning;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setPrijs(Double prijs) {
        this.prijs = prijs;
    }

    public void setStudent(Boolean student) {
        isStudent = student;
    }

    public void setAfrekening(Afrekening afrekening) {
        this.afrekening = afrekening;
    }

    public void setZetel(Zetel zetel) {
        this.zetel = zetel;
    }

    public void setVertoning(Vertoning vertoning) {
        this.vertoning = vertoning;
    }

    public Integer getId() {
        return id;
    }

    public String getBarcode() {
        return barcode;
    }

    public Double getPrijs() {
        return prijs;
    }

    public Boolean getStudent() {
        return isStudent;
    }

    public Afrekening getAfrekening() {
        return afrekening;
    }

    public Zetel getZetel() {
        return zetel;
    }

    public Vertoning getVertoning() {
        return vertoning;
    }
}
