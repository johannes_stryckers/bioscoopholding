package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:58
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "T_AFREKENING")
public class Afrekening {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private Date datum;
    private double prijs;

    @OneToMany(mappedBy = "afrekening")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<TienBeurtenTicket> tienBeurtenTickets = new ArrayList<TienBeurtenTicket>();

    @OneToMany(mappedBy = "afrekening")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Ticket> tickets = new ArrayList<Ticket>();

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "verkoopPuntId")
    private VerkoopPunt verkoopPunt;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "bezoekerId")
    private Bezoeker bezoeker;

    public Afrekening() {
    }

    public Afrekening(Date datum) {
        this.datum = datum;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public Date getDatum() {
        return datum;
    }

    public double getPrijs() {
        return prijs;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public void setPrijs(double prijs) {
        this.prijs = prijs;
    }

    public VerkoopPunt getVerkoopPunt() {
        return verkoopPunt;
    }

    public Bezoeker getBezoeker() {
        return bezoeker;
    }

    public void setVerkoopPunt(VerkoopPunt verkoopPunt) {
        this.verkoopPunt = verkoopPunt;
    }

    public void setBezoeker(Bezoeker bezoeker) {
        this.bezoeker = bezoeker;
    }

    public void addTicket(Ticket ticket){
        ticket.setAfrekening(this);
        tickets.add(ticket);
        prijs +=ticket.getPrijs();
    }

    public void addTienbeurtenTicket(TienBeurtenTicket tienBeurtenTicket){
        tienBeurtenTicket.setAfrekening(this);
        tienBeurtenTickets.add(tienBeurtenTicket);
    }
}
