package Model;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:53
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "T_VERKOOPPUNT")
public class VerkoopPunt {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private Integer nr;

    @ManyToOne
    @JoinColumn(name="cinemacomplexId")
    private CinemaComplex cinemaComplex;

    public VerkoopPunt() {
    }

    public VerkoopPunt(Integer nr) {
        this.nr = nr;
    }

    public Integer getNr() {
        return nr;
    }

    public void setNr(Integer nr) {
        this.nr = nr;
    }

    public CinemaComplex getCinemaComplex() {
        return cinemaComplex;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCinemaComplex(CinemaComplex cinemaComplex) {
        this.cinemaComplex = cinemaComplex;
    }
}
