package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_TIENBEURTENTICKET")
public class TienBeurtenTicket {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private Date datum;
    private Double prijs;
    private Integer geldigheidsDuur;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "afrekeningId")
    private Afrekening afrekening;

    public TienBeurtenTicket() {
    }

    public Integer getId() {
        return id;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Double getPrijs() {
        return prijs;
    }

    public void setPrijs(Double prijs) {
        this.prijs = prijs;
    }

    public Integer getGeldigheidsDuur() {
        return geldigheidsDuur;
    }

    public void setGeldigheidsDuur(Integer geldigheidsDuur) {
        this.geldigheidsDuur = geldigheidsDuur;
    }

    public Afrekening getAfrekening() {
        return afrekening;
    }

    public void setAfrekening(Afrekening afrekening) {
        this.afrekening = afrekening;
    }
}
