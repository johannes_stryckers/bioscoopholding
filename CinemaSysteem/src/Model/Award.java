package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_AWARD")
public class Award {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String naam;
    private Date datum;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "filmId")
    private Film film;

    public Award() {
    }

    public Award(String naam, Date datum) {
        this.naam = naam;
        this.datum = datum;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Integer getId() {

        return id;
    }

    public String getNaam() {
        return naam;
    }

    public Date getDatum() {
        return datum;
    }

    public Film getFilm() {
        return film;
    }
}
