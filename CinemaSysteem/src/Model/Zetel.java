package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "T_ZETEL")
public class Zetel {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private Integer zetelNr;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "zoneId")
    private Zone zone;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "zaalId")
    private Zaal zaal;

    public Zetel() {
    }

    public Zaal getZaal() {
        return zaal;
    }

    public void setZaal(Zaal zaal) {
        this.zaal = zaal;
    }

    public Zetel(Integer zetelNr) {
        this.zetelNr = zetelNr;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setZetelNr(Integer zetelNr) {
        this.zetelNr = zetelNr;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public Integer getId() {
        return id;
    }

    public Integer getZetelNr() {
        return zetelNr;
    }

    public Zone getZone() {
        return zone;
    }

    @Override
    public String toString() {
        return String.format("Zetel: %d, Zone: %s Zaal: %d", zetelNr, zone.getNaam(), zaal.getZaalNr());
    }
}
