package Model;

import org.hibernate.annotations.Cascade;
import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_FUNCTIE")
public class Functie {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String naam;
    private String type;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "filmId")
    private Film film;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "medewerkerId")
    private Medewerker medewerker;

    public Functie() {
    }

    public Functie(String naam, String type) {
        this.naam = naam;
        this.type = type;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public void setMedewerker(Medewerker medewerker) {
        this.medewerker = medewerker;
    }

    public Integer getId() {
        return id;
    }

    public String getNaam() {
        return naam;
    }

    public String getType() {
        return type;
    }

    public Film getFilm() {
        return film;
    }

    public Medewerker getMedewerker() {
        return medewerker;
    }
}
