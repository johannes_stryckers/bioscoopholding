package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:53
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_CINEMACOMPLEX")
public class CinemaComplex {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String naam;
    private String adres;

    @OneToMany(mappedBy = "cinemaComplex")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<VerkoopPunt> verkoopPunten = new ArrayList<VerkoopPunt>();

    @OneToMany(mappedBy = "cinemaComplex")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Zaal> zalen = new ArrayList<Zaal>();

    public CinemaComplex() {
    }

    public CinemaComplex(String naam, String adres) {
        this.naam = naam;
        this.adres = adres;
    }

    public Integer getId() {
        return id;
    }

    public String getNaam() {
        return naam;
    }

    public String getAdres() {
        return adres;
    }

    public List<VerkoopPunt> getVerkoopPunten() {
        return verkoopPunten;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public void addVerkoopPunt(VerkoopPunt verkoopPunt){
        verkoopPunt.setCinemaComplex(this);
        verkoopPunten.add(verkoopPunt);
    }

    public void addZaal(Zaal zaal){
        zaal.setCinemaComplex(this);
        zalen.add(zaal);
    }

    @Override
    public String toString() {
        return naam;
    }

    public List<Zaal> getZalen() {
        return zalen;
    }
}
