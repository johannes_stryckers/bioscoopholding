package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:58
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table (name="T_UITGEVER")
public class Uitgever {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String naam;
    private Date oprichtingsJaar;
    private String adres;

    @OneToMany(mappedBy = "uitgever")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Film> films = new ArrayList<Film>();

    public Uitgever() {
    }

    public Uitgever(String adres, String naam, Date oprichtingsJaar) {
        this.adres = adres;
        this.naam = naam;
        this.oprichtingsJaar = oprichtingsJaar;
    }

    public Integer getId() {
        return id;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Date getOprichtingsJaar() {
        return oprichtingsJaar;
    }

    public void setOprichtingsJaar(Date oprichtingsJaar) {
        this.oprichtingsJaar = oprichtingsJaar;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public void addFilm(Film film){
        film.setUitgever(this);
        films.add(film);
    }

    @Override
    public String toString() {
        return naam;
    }
}
