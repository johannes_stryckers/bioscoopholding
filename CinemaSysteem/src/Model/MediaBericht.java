package Model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 16/10/12
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "T_MEDIABERICHT")
public class MediaBericht {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String filmNaam;
    private String sociaalNetwerk;

    @Column(columnDefinition="TEXT")
    private String inhoud;
    private Date datum;

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "filmId")
    private Film film;

    public MediaBericht() {
    }

    public String getSociaalNetwerk() {
        return sociaalNetwerk;
    }

    public void setSociaalNetwerk(String sociaalNetwerk) {
        this.sociaalNetwerk = sociaalNetwerk;
    }

    public String getFilmNaam() {
        return filmNaam;
    }

    public void setFilmNaam(String filmNaam) {
        this.filmNaam = filmNaam;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public void setInhoud(String inhoud) {
        this.inhoud = inhoud;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Integer getId() {

        return id;
    }

    public String getInhoud() {
        return inhoud;
    }

    public Date getDatum() {
        return datum;
    }

    public Film getFilm() {
        return film;
    }
}
