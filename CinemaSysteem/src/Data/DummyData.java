package Data;

import Model.*;
import Persistence.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Bart
 * Date: 23/10/12
 * Time: 14:27
 * To change this template use File | Settings | File Templates.
 */
public class DummyData {
    public DummyData() {

        //Cinemacomplexen + zalen, stoelen en verkooppunten aanmaken
        List<CinemaComplex> complexen = new ArrayList<CinemaComplex>();
        List<Medewerker> medewerkers = new ArrayList<Medewerker>();
        List<Medewerker> acteurs = new ArrayList<Medewerker>();
        List<Bezoeker> bezoekers = new ArrayList<Bezoeker>();
        List<Uitgever> uitgevers = new ArrayList<Uitgever>();
        List<Zone> zones = new ArrayList<Zone>();
        List<Functie> functies = new ArrayList<Functie>();
        String[] roles = {"Acteur","Regisseur","Muzikant","Componist","Schrijver"};
        String korteInhoud = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.";
        String[] genres = {"Actie","Horror","Thriller","Drama","Komedie","Western","Oorlog"};
        List<Award> awards = new ArrayList<Award>();
        List<Film> films = new ArrayList<Film>();
        String separator =System.getProperty("file.separator") ;
        List<Afrekening> afrekeningen = new ArrayList<Afrekening>();

        zones.add(new Zone("Handicap", 8.5));
        zones.add(new Zone("Regular", 9.0));
        zones.add(new Zone("Love Seat", 10.0));
        
        complexen.add(new CinemaComplex("Metropolis Antwerpen", "Groenendaallaan 394 Antwerpen 2030"));
        complexen.add(new CinemaComplex("Utopolis Mechelen", "Spuibeekstraat 5 Mechelen 2800"));
        complexen.add(new CinemaComplex("Kinepolis Leuven", "Bondgenotenlaan 145-149 Leuven 3000"));
        complexen.add(new CinemaComplex("Kinepolis Oostende", "Koningin Astridlaan 12 Oostende 8400"));

        for(CinemaComplex complex: complexen){
            for(int i=1; i<12; i++){
                Zaal zaal = new Zaal(i);
                int j=1;
                for(Zone zone: zones){
                    if(zone.getNaam().equals("Handicap")){
                        for(; j<=20; j++){
                            Zetel zetel = new Zetel();
                            zetel.setZetelNr(j);
                            zetel.setZone(zone);
                            zaal.addZetel(zetel);
                        }
                    }else if(zone.getNaam().equals("Regular")){
                        for(; j<=80; j++){
                            Zetel zetel = new Zetel();
                            zetel.setZetelNr(j);
                            zetel.setZone(zone);
                            zaal.addZetel(zetel);
                        }
                    }else{
                        for(; j<=100; j++){
                            Zetel zetel = new Zetel();
                            zetel.setZetelNr(j);
                            zetel.setZone(zone);
                            zaal.addZetel(zetel);
                        }
                    }
                }
                complex.addZaal(zaal);
            }
            for(int k=1; k<=4; k++){
                complex.addVerkoopPunt(new VerkoopPunt(k));
            }
        }

        //Bezoekers aanmaken
        try{
        File file = new File("CinemaSysteem"+separator+"src"+separator+"Data"+separator+"Files"+separator+"Klanten.csv");
        BufferedReader bufRdr  = new BufferedReader(new FileReader(file));
        String line = null;

        //read each line of text file
        while((line = bufRdr.readLine()) != null)
        {
            Bezoeker bezoeker = new Bezoeker();
            StringTokenizer st = new StringTokenizer(line,";");
            bezoeker.setNaam(st.nextToken());
            bezoeker.setStudent(st.nextToken().equals("true"));
            bezoeker.setAdres(st.nextToken());
            bezoekers.add(bezoeker);
        }

        bufRdr.close();

        }catch(Exception e){
            e.printStackTrace();
        }

        //Uitgevers aanmaken
        uitgevers.add(new Uitgever("7275 Elit. Rd. 27551 Anderson El Salvador","Fox Entertainment Group",new Date("10/11/2008")));
        uitgevers.add(new Uitgever("Ap #896-5570 Phasellus Avenue 66688 Mechanicville Bahrain"," Paramount Motion Pictures Group",new Date("09/26/2006")));
        uitgevers.add(new Uitgever("965-739 Tempus Ave 75148 Idaho Springs El Salvador","DreamWorks",new Date("9/02/2008")));
        uitgevers.add(new Uitgever("476-2182 Proin Avenue 39783 Hanahan Latvia","Sony Pictures Entertainment",new Date("2/04/2010")));
        uitgevers.add(new Uitgever("562-2989 Aliquam St. 27921 Oil City Suriname","MGM Holdings Inc.",new Date("05/20/2004")));
        uitgevers.add(new Uitgever("Ap #732-3927 Nisl St. 68486 Santa Clarita Somalia","NBC Universal",new Date("12/29/2009")));
        uitgevers.add(new Uitgever("P.O. Box 776 1722 Quisque Ave 94624 Seattle Aruba"," Time Warner Bros",new Date("2/01/2005")));
        uitgevers.add(new Uitgever("P.O. Box 982 9279 Penatibus St. 15146 San Marino French Polynesia","Walt Disney Motion Pictures Group",new Date("6/03/2012")));

        //Medewerkers aanmaken
        try{
            File file = new File("CinemaSysteem"+separator+"src"+separator+"Data"+separator+"Files"+separator+"Medewerkers.csv");

            BufferedReader bufRdr  = new BufferedReader(new FileReader(file));
            String line = null;

            //read each line of text file
            while((line = bufRdr.readLine()) != null)
            {
                Medewerker medewerker = new Medewerker();
                StringTokenizer st = new StringTokenizer(line,";");
                medewerker.setNaam(st.nextToken());
                medewerker.setGeboorteDatum(new Date(st.nextToken()));
                medewerkers.add(medewerker);
            }

            bufRdr.close();

        }catch(Exception e){}

        //Acteurs aanmaken
        try{
            File file = new File("CinemaSysteem"+separator+"src"+separator+"Data"+separator+"Files"+separator+"Acteurs.csv");

            BufferedReader bufRdr  = new BufferedReader(new FileReader(file));
            String line = null;

            //read each line of text file
            while((line = bufRdr.readLine()) != null)
            {
                Medewerker medewerker = new Medewerker();
                StringTokenizer st = new StringTokenizer(line,";");
                medewerker.setNaam(st.nextToken());
                medewerker.setGeboorteDatum(new Date(st.nextToken()));
                acteurs.add(medewerker);
            }

            bufRdr.close();

        }catch(Exception e){}

        //maak 8000 functies aan
        try{
            File file = new File("CinemaSysteem"+separator+"src"+separator+"Data"+separator+"Files"+separator+"Names.csv");
            FileReader reader = new FileReader(file);
            BufferedReader bufRdr = new BufferedReader(reader);
            Random rnd = new Random();

            for(int i=1; i<=6000; i++){
                if(i%1000 == 0){
                    reader = new FileReader(file);
                    bufRdr = new BufferedReader(reader);
                }
                Functie functie = new Functie(bufRdr.readLine(), roles[rnd.nextInt(roles.length)]);
                functie.setMedewerker(medewerkers.get(rnd.nextInt(medewerkers.size())));
                functies.add(functie);
            }
            for(int i=1; i<=2000; i++){
                if(i%1000 == 0){
                    reader = new FileReader(file);
                    bufRdr = new BufferedReader(reader);
                }
                Functie functie = new Functie(bufRdr.readLine(), roles[0]);
                functie.setMedewerker(acteurs.get(rnd.nextInt(acteurs.size())));
                functies.add(functie);
            }
            bufRdr.close();

        }catch(Exception e){
            e.printStackTrace();
        }

        //Maak Awards
        try{
            File file = new File("CinemaSysteem"+separator+"src"+separator+"Data"+separator+"Files"+separator+"Awards.csv");
            
            for(int i=1; i<=10; i++){
                BufferedReader bufRdr  = new BufferedReader(new FileReader(file));
                for(int j=0; j<16; j++){
                    Award award = new Award(bufRdr.readLine(), new Date(100+i, 1, 1));
                    awards.add(award);
                }
                bufRdr.close();
            }

        }catch(Exception e){}

        //Maak Films
        long offset = Timestamp.valueOf("2000-01-01 00:00:00").getTime();
        long end = Timestamp.valueOf("2012-12-31 00:00:00").getTime();
        long diff = end - offset + 1;
        try{
            File file = new File("CinemaSysteem"+separator+"src"+separator+"Data"+separator+"Files"+separator+"FilmTitels.csv");
            ArrayList<String> titels = new ArrayList<String>();
            
            BufferedReader bufRdr  = new BufferedReader(new FileReader(file));
            String line = null;
            while((line = bufRdr.readLine()) != null)
            {
                titels.add(line);
            }

            file = new File("CinemaSysteem"+separator+"src"+separator+"Data"+separator+"Files"+separator+"Landen.csv");
            ArrayList<String> landen = new ArrayList<String>();

            bufRdr  = new BufferedReader(new FileReader(file));
            line = null;
            while((line = bufRdr.readLine()) != null)
            {
                landen.add(line);
            }
            
            Random rnd = new Random();
            int j=0;
            for(int i=1; i<=4000; i++){
                int titel = rnd.nextInt(titels.size());
                Film film = new Film(titels.get(titel), 80 + (int)(Math.random() * ((160 - 80) + 1)), 6 + (int)(Math.random() * ((21 - 6) + 1)), landen.get(rnd.nextInt(landen.size())), korteInhoud, genres[rnd.nextInt(genres.length)], i%15==0?true:false);
                titels.remove(titel);
                film.setUitgever(uitgevers.get(rnd.nextInt(uitgevers.size())));
                Timestamp rand = new Timestamp(offset + (long)(Math.random() * diff));
                Date date = new Date(rand.getYear(),rand.getMonth(),rand.getDay());
                film.setDate(date);
                int functie = rnd.nextInt(functies.size());
                film.addFunctie(functies.get(functie));
                functies.remove(functie);
                functie = rnd.nextInt(functies.size());
                film.addFunctie(functies.get(functie));
                functies.remove(functie);
                if(i%25 == 0){
                    film.addAward(awards.get(j++));
                }

                films.add(film);
            }



        }catch(Exception e){
            e.printStackTrace();
        }

        //Maak vertoningen
        offset = Timestamp.valueOf("2012-11-01 00:00:00").getTime();
        end = Timestamp.valueOf("2012-12-31 00:00:00").getTime();
        diff = end - offset + 1;

        for(int i=0; i<40; i++){
            Film film = films.get(i*43);
            for(int j=0; j<40; j++){
                Timestamp rand = new Timestamp(offset + (long)(Math.random() * diff));
                Date date = new Date(rand.getYear(),rand.getMonth(),rand.getDay());
                date.setHours(14 + (int) (Math.random() * ((23 - 14) + 1)));
                Vertoning vertoning = new Vertoning(date, j%3==0?true:false, j%5==0?true:false, j%2==0?true:false);
                vertoning.setZaal(complexen.get(j%4).getZalen().get(j/4));
                film.addVertoning(vertoning);
            }
        }

        //Maak tickets en afrekeningen
        //Eerst een gecontroleerde afrekening
        Date date = new Date(112,11,10);
        date.setHours(20);
        Vertoning vertoning = new Vertoning(date, true, true, true);
        vertoning.setZaal(complexen.get(0).getZalen().get(0));
        Afrekening afrekening = new Afrekening();
        int k = 0;
        for(int i=0; i<12; i++){
            Zetel zetel = vertoning.getZaal().getZetels().get(i+15);
            Ticket ticket = new Ticket();
            ticket.setStudent(false);
            ticket.setAfrekening(afrekening);
            ticket.setBarcode((new Date().getTime()) + "" + k++);
            ticket.setZetel(zetel);
            ticket.setPrijs(zetel.getZone().getPrijs() + 2);
            vertoning.addTicket(ticket);
            afrekening.setPrijs(afrekening.getPrijs()+ticket.getPrijs());
        }
        afrekening.setDatum(new Date());
        films.get(3500).addVertoning(vertoning);
        //Random afrekeningen
        Random rnd = new Random();
        for(Film film: films){
            for(Vertoning v: film.getVertoningen()){
                List<Integer> zetels = new ArrayList<Integer>();
                for(int i=0; i<100; i++){
                    zetels.add(i);
                }
                for(int j=0; j<=rnd.nextInt(7)+8; j++){
                    Afrekening a = new Afrekening();
                    for(int i=0; i<=rnd.nextInt(5)+3; i++){
                        int z = rnd.nextInt(zetels.size());
                        Zetel zetel = v.getZaal().getZetels().get(zetels.get(z));
                        zetels.remove(z);
                        Ticket ticket = new Ticket();
                        ticket.setStudent(j%2==0?true:false);
                        ticket.setBarcode((new Date().getTime()) + "" + k++);
                        ticket.setZetel(zetel);
                        ticket.setPrijs(zetel.getZone().getPrijs() + (v.isDrieD()?2:0) - (ticket.getStudent()?2:0));
                        a.setPrijs(a.getPrijs()+ticket.getPrijs());
                        ticket.setAfrekening(a);
                        v.addTicket(ticket);
                    }
                    Timestamp rand = new Timestamp(offset + (long)(Math.random() * diff));
                    date = new Date(rand.getYear(),rand.getMonth(),rand.getDay());
                    a.setDatum(date);
                    a.setVerkoopPunt(v.getZaal().getCinemaComplex().getVerkoopPunten().get(rnd.nextInt(v.getZaal().getCinemaComplex().getVerkoopPunten().size())));
                    if(j%2==0){
                        a.setBezoeker(bezoekers.get(rnd.nextInt(bezoekers.size())));
                    }
                    afrekeningen.add(a);
                }
            }
        }

        //Maak reviews
        offset = Timestamp.valueOf("2002-01-01 00:00:00").getTime();
        end = Timestamp.valueOf("2012-12-31 00:00:00").getTime();
        diff = end - offset + 1;
        for(int i=0; i<250; i++){
            Timestamp rand = new Timestamp(offset + (long)(Math.random() * diff));
            date = new Date(rand.getYear(),rand.getMonth(),rand.getDay());
            Review review = new Review();
            review.setDatum(date);
            review.setQuotering(rnd.nextInt(6));
            review.setFilm(films.get(i * 16));
            review.setInhoud(korteInhoud);
            bezoekers.get(i*4).addReview(review);
        }

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = session.beginTransaction();
        for(Film film: films){
            session.saveOrUpdate(film);
        }
        for(Bezoeker bezoeker: bezoekers){
            session.saveOrUpdate(bezoeker);
        }
        for(Afrekening af: afrekeningen){
            session.saveOrUpdate(af);
        }
        tx.commit();
    }
}
