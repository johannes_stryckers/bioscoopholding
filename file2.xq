xquery version "1.0";<programmatie> 
{let $nl := "&#10;" (: nieuwe lijn :)
for $datum in distinct-values(//CinemaComplex[@Name="Metropolis Antwerpen"]/Vertoning/@Datum) 
where ($datum >= '2012-11-01') and ($datum < '2012-11-08') 
order by $datum 
return (<dag datum="{substring($datum,1,10)}"> {
for $vertoning in //CinemaComplex[@Name="Metropolis Antwerpen"]/Vertoning[@Datum = $datum] 

return (<film titel="{$vertoning/@FilmNaam}" zaal="{$vertoning/@Zaal}" tijd="{substring($vertoning/@Datum,12,5)}" />, $nl)} 
</dag>, $nl)} </programmatie>