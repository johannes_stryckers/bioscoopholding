for $vertoning in /CinemaHolding/CinemaComplex[@Name="Utopolis Mechelen"]/Vertoning
let $dag := $vertoning/@Datum
let $film := data($vertoning/@FilmNaam)
where xs:date(substring($dag,1,10)) <= xs:date("2012-11-05")
return $film