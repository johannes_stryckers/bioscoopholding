xquery version "1.0";
let $nl := "&#10;" (: nieuwe lijn :)
for $vertoning in //CinemaComplex[@Name="Metropolis Antwerpen"]/Vertoning[@Zaal = 8] 
where ($vertoning/@Datum >= '2012-11-01') and ($vertoning/@Datum < '2012-11-08') 
order by $vertoning/@Datum 
return (<film titel="{$vertoning/@FilmNaam}" uur="{substring($vertoning/@Datum,12,5)}" />, $nl)